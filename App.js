import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import List from './src/List';
import Details from './src/Details';


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="List">
        <Stack.Screen name="List" component={List}/>
        <Stack.Screen name="Details" component={Details} options={{title: "informações dos usuarios"}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}



