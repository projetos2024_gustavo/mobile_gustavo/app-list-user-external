import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const Details = ({ route }) => {
  const { user } = route.params;

  return (
    <View>
      <Text>Lista de Usuários </Text>
      <Text>Nome: {user.name}</Text>
      <Text>ID : {user.id}</Text>
      <Text>Username : {user.username}</Text>
      <Text>email : {user.email}</Text>
      <Text>Rua : {user.address.street}</Text>
      <Text>Suíte : {user.address.suite}</Text>
      <Text>Cidade : {user.address.city}</Text>
      <Text>Zipcode : {user.address.zipcode}</Text>
      <Text>Latitude : {user.address.geo.lat}</Text>
      <Text>Longitude : {user.address.geo.lng}</Text>
    </View>
  );
};

export default Details;
